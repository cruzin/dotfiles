local group = vim.api.nvim_create_augroup('cruzin', { clear = true })

-- Equalize splits on resize
vim.api.nvim_create_autocmd('VimResized', {
  group = group,
  command = 'tabdo wincmd ='
})

-- Fix all ESLint issues on save
vim.api.nvim_create_autocmd('BufWritePre', {
  group = group,
  pattern = { '*.tsx', '*.ts', '*.jsx', '*.js' },
  command = 'EslintFixAll'
})

-- Automatically reload neovim when we edit plugins.lua
vim.api.nvim_create_autocmd('BufWritePost', {
  group = group,
  pattern = 'plugins.lua',
  command = 'source <afile> | PackerSync'
})

-- Set wrap/spell on some filetypes
vim.api.nvim_create_autocmd('FileType', {
  group = group,
  pattern = { 'text', 'gitcommit', 'markdown' },
  callback = function()
    vim.opt_local.wrap = true
    vim.opt_local.spell = true
  end
})

-- Highlight yanked text
vim.api.nvim_create_autocmd('TextYankPost', {
  group = group,
  callback = function()
    vim.highlight.on_yank({ higroup = 'Folded', timeout = 350 })
  end
})

-- Check if a buffer has been updated outside of neovim
vim.api.nvim_create_autocmd('BufWinEnter', {
  group = group,
  pattern = { '*' },
  command = 'checktime'
})
