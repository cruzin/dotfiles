local M = {}
vim.notify = require('notify')

M.defer = function(fn, timeout)
  if vim.in_fast_event() then
    vim.schedule(fn)
  else
    vim.defer_fn(fn, timeout or 100)
  end
end

M.plugin_fail = function(plugins)
  plugins = vim.tbl_map(function(plugin) return ' • ' .. plugin end, plugins)
  local term = #plugins > 1 and 'Some plugins' or 'A plugin'
  local msg = term .. ' failed to load correctly: \n' .. table.concat(plugins, '\n')
  vim.notify(msg, vim.log.levels.ERROR, {
    title = 'Plugin Load Failure',
    on_open = function(win)
      local buf = vim.api.nvim_win_get_buf(win)
      vim.api.nvim_buf_set_option(buf, 'filetype', 'bat')
    end
  })
end

M.get_plugins_list = function()
  local short_name = require('packer.util').get_plugin_short_name
  local list_plugins = require('packer.plugin_utils').list_installed_plugins

  local opt, start = list_plugins()
  local plugin_paths = vim.tbl_extend('force', opt, start)
  local plugins = {}

  for path in pairs(plugin_paths) do
    local name, _ = short_name({ path }):gsub('.nvim', '')
    table.insert(plugins, name)
  end

  table.sort(plugins)

  return plugins
end

M.wrap_line = function(line, max_len)
  local formatted_line = {}

  local words = {}
  for word in line:gmatch('%S+') do
    table.insert(words, word)
  end

  local buffer = ''
  for i, word in ipairs(words) do
    if (#buffer + #word) <= (max_len or 50) then
      buffer = buffer .. word .. ' '

      if i == #words then
        table.insert(formatted_line, buffer:sub(1, -2))
      end
    else
      table.insert(formatted_line, buffer:sub(1, -2))
      buffer = '' .. word .. ' '

      if i == #words then
        table.insert(formatted_line, buffer:sub(1, -2))
      end
    end
  end

  return formatted_line
end

M.kind_icons = {
  Array = { icon = '', hl = '@variable.builtin' },
  Boolean = { icon = '', hl = 'Boolean' },
  Class = { icon = '', hl = 'Type' },
  Color = { icon = '', hl = 'Constant' },
  Constant = { icon = '', hl = 'Constant' },
  Constructor = { icon = '', hl = '@constructor' },
  Enum = { icon = '', hl = 'Constant' },
  EnumMember = { icon = '', hl = 'Constant' },
  Event = { icon = '', hl = 'PrePoc' },
  Field = { icon = 'ﰉ', hl = 'Define' },
  File = { icon = '', hl = 'Label' },
  Function = { icon = '', hl = 'Function' },
  Interface = { icon = 'כֿ', hl = 'Type' },
  Key = { icon = '', hl = 'Keyword' },
  Keyword = { icon = '', hl = 'Keyword' },
  Method = { icon = '', hl = 'Function' },
  Module = { icon = '', hl = 'Include' },
  Namespace = { icon = '', hl = 'Identifier' },
  Null = { icon = '', hl = 'NonText' },
  Number = { icon = '', hl = 'Number' },
  Object = { icon = '', hl = 'Structure' },
  Operator = { icon = '', hl = 'Operator' },
  Package = { icon = '', hl = 'Structure' },
  Property = { icon = '', hl = '@property' },
  Reference = { icon = '', hl = 'Normal' },
  Snippet = { icon = '', hl = 'String' },
  String = { icon = '', hl = 'String' },
  Struct = { icon = '', hl = 'Structure' },
  Text = { icon = '', hl = 'Normal' },
  TypeParameter = { icon = '', hl = 'Type' },
  Unit = { icon = '', hl = 'String' },
  Value = { icon = '', hl = 'Normal' },
  Variable = { icon = '[]', hl = '@variable.builtin' },
}

return M
