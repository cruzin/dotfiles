local plugin = 'telescope'
local tl_ok, tl = pcall(require, plugin)

if not tl_ok then
  require('utils').plugin_fail({ plugin })
  return
end

local actions = require('telescope.actions')
local builtin = require('telescope.builtin')
local previewers = require('telescope.previewers')

-- Don't show preview for large files
local limit_maker = function(filepath, bufnr, opts)
  opts = opts or {}

  filepath = vim.fn.expand(filepath)

  vim.loop.fs_stat(filepath, function(_, stat)
    if not stat then return end
    if stat.size > 100000 then
      return
    else
      previewers.buffer_previewer_maker(filepath, bufnr, opts)
    end
  end)
end

local ignore_patterns = {
  '^.git/', '^.vscode/', '^node_modules/'
}

tl.setup({
  defaults = {
    buffer_previewer_maker = limit_maker,
    file_ignore_patterns = ignore_patterns,
    multi_icon = ' ',
    prompt_prefix = ' ',
    selection_caret = ' '
  },
  pickers = {
    buffers = { theme = 'dropdown' },
    builtin = { theme = 'ivy' },
    command_history = { theme = 'ivy' },
    commands = { theme = 'ivy' },
    current_buffer_fuzzy_find = { theme = 'ivy' },
    diagnostics = { theme = 'dropdown' },
    file_browser = { theme = 'dropdown', hidden = true },
    find_files = { theme = 'dropdown', hidden = true },
    git_branches = { theme = 'ivy' },
    git_files = { theme = 'dropdown', hidden = true },
    help_tags = { theme = 'ivy' },
    live_grep = { theme = 'dropdown' },
    man_pages = { theme = 'ivy' },
    marks = { theme = 'dropdown' },
    oldfiles = { theme = 'dropdown', hidden = true },
    treesitter = { theme = 'dropdown' },
  },
  extensions = {
    repo = {
      list = {
        search_dirs = {
          '~/Projects',
        }
      }
    },
    file_browser = {
      dir_icon = '',
      dir_icon_hl = 'Directory',
      grouped = true, -- List dirs first
      hidden = true, -- Always show hidden files
      hijack_netrw = true,
      theme = 'ivy',
    }
  }
})

tl.load_extension('repo')
tl.load_extension('projects')
tl.load_extension('file_browser')

require('which-key').register({
  f = {
    name = 'Find',
    C = { builtin.command_history, 'Command History' },
    H = { builtin.help_tags, 'Help Tags' },
    M = { builtin.man_pages, 'Man Pages' },
    P = { builtin.builtin, 'Telescope Pickers' },
    b = { builtin.buffers, 'Buffers' },
    c = { builtin.commands, 'Commands' },
    d = { ':Telescope file_browser<cr>', 'File Browser' },
    f = { builtin.find_files, 'Files' },
    g = { builtin.live_grep, 'Live Grep' },
    m = { builtin.marks, 'Marks' },
    p = { ':Telescope projects<cr>', 'Projects' },
    s = { builtin.treesitter, 'Symbols' },
    t = { ':TodoTelescope<cr>', 'Todos' },
    z = { builtin.current_buffer_fuzzy_find, 'Buffer Fuzzy Find' },
  },
  g = {
    name = 'Git',
    b = { builtin.git_branches, 'Git Branches' },
    f = { builtin.git_files, 'Git Files' },
  },
}, { prefix = '<leader>' })
