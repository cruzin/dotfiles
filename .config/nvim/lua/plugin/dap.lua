local dap_ok, dap = pcall(require, 'dap')
local dvt_ok, dvt = pcall(require, 'nvim-dap-virtual-text')
local vscode_ok, vscode = pcall(require, 'dap-vscode-js')

if not dap_ok
  or not dvt_ok
  or not vscode_ok then
  require('utils').plugin_fail({
    'dap', 'nvim-dap-virtual-text', 'dap-vscode-js'
  })
  return
end

-- Define how breakpoints look
local option = {
  breakpoint = {
    text = '',
    texthl = 'DiagnosticSignError',
    linehl = '',
    numhl = '',
  },
  breakpoint_condition = {
    text = '',
    texthl = 'DiagnosticSignError',
    linehl = '',
    numhl = '',
  },
  breakpoint_rejected = {
    text = '',
    texthl = 'LspDiagnosticsSignHint',
    linehl = '',
    numhl = '',
  },
  logpoint = {
    text = '',
    texthl = 'LspDiagnosticsSignHint',
    linehl = '',
    numhl = '',
  },
  stopped = {
    text = '',
    texthl = 'DiagnosticSignWarn',
    linehl = 'Visual',
    numhl = 'DiagnosticSignWarn',
  }
}

vim.fn.sign_define('DapBreakpoint', option.breakpoint)
vim.fn.sign_define('DapBreakpointCondition', option.breakpoint_condition)
vim.fn.sign_define('DapBreakpointRejected', option.breakpoint_rejected)
vim.fn.sign_define('DapStopped', option.stopped)
vim.fn.sign_define('DapLogPoint', option.logpoint)

-- EXTENSIONS
-- Virtual text
dvt.setup({})

-- DAP
-- Define adapters/configs 
-- TODO: Find and set up a working Firefox adapter/config
vscode.setup({
  adapters = { 'pwa-node', 'pwa-chrome' }
})

-- NOTE: Type should match adapter
for _, language in ipairs({
  'typescript',
  'javascript',
  'typescriptreact',
  'javascriptreact'
}) do
  dap.configurations[language] = {
    {
      name = 'JEST: Debug Tests',
      type = 'pwa-node',
      request = 'launch',
      -- trace = true, -- include debugger info
      runtimeExecutable = 'node',
      runtimeArgs = {
        vim.fn.getcwd() .. './node_modules/jest/bin/jest.js', -- FIX: change cwd?
        '--runInBand',
      },
      rootPath = '${workspaceFolder}',
      cwd = vim.fn.getcwd(),
      console = 'integratedTerminal',
      internalConsoleOptions = 'neverOpen',
      --sourcemaps = false,
    },
    {
      name = 'NODE: Launch file',
      type = 'pwa-node',
      request = 'launch',
      program = '${file}',
      cwd = vim.fn.getcwd(),
      sourcemaps = true,
      console = 'integratedTerminal',
      protocol = 'inspector'
    },
  }
end

dap.set_log_level('WARN')

local M = {}

M.set_conditional_breakpoint = function()
  dap.set_breakpoint(vim.fn.input('Breakpoint condition: '))
end

M.set_log_point = function()
  dap.set_breakpoint(nil, nil, vim.fn.input('Log Point Message: '))
end

M.show_context = function()
  require('dap.ui.widgets').hover()
end

-- Set up keymap
require('which-key').register({
  d = {
    name = 'Debug',
    B = { M.set_conditional_breakpoint, 'Set Conditional Breakpoint' },
    L = { M.set_log_point, 'Set Log Point' },
    _ = { dap.clear_breakpoints, 'Clear Breakpoints' },
    a = { ':Telescope dap list_breakpoints<cr>', 'List All Breakpoints' },
    b = { dap.toggle_breakpoint, 'Toggle Breakpoint' },
    c = { dap.continue, 'Start Debugger' },
    l = { dap.run_last, 'Run Last Debugger' },
    w = { M.show_context, 'Show Context' },
    x = { dap.terminate, 'End Debugger' },
  },
}, { prefix = '<leader>' })

require('which-key').register({
    ['<F2>'] = { dap.step_back, 'Step Back' },
    ['<F3>'] = { dap.step_over, 'Step Over' },
    ['<F4>'] = { dap.step_out, 'Step Out' },
    ['<F5>'] = { dap.step_into, 'Step Into' },
})
