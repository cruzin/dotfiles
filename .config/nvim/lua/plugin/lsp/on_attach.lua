local function print_workspace_folders()
  print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
end

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local function on_attach(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
  vim.api.nvim_buf_set_option(bufnr, 'tagfunc', 'v:lua.vim.lsp.tagfunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  require('which-key').register({
    l = {
      name = 'LSP',
      s = { ':SymbolsOutline<cr>', 'Document Symbols' },
    },
    w = {
      name = 'Workspace Folders',
      a = { vim.lsp.buf.add_workspace_folder, 'Add Workspace Folder' },
      r = { vim.lsp.buf.remove_workspace_folder, 'Remove Workspace Folder' },
      l = { print_workspace_folders, 'List Workspace Folders' },
    },
    D = { vim.lsp.buf.type_definition, 'Go to Type Definition' },
    rn = { vim.lsp.buf.rename, 'Rename' },
    ca = { vim.lsp.buf.code_action, 'Code Action' },
  }, { prefix = '<leader>' })

  local function next()
    vim.diagnostic.goto_next({ float = false })
  end

  local function prev()
    vim.diagnostic.goto_prev({ float = false })
  end

  require('which-key').register({
    gD = { vim.lsp.buf.declaration, 'Go to Declaration' },
    gd = { vim.lsp.buf.definition, 'Go to Definition' },
    K  = { vim.lsp.buf.hover, 'Show Description' },
    gi = { vim.lsp.buf.implementation, 'Go to Implementation' },
    gr = { vim.lsp.buf.references, 'List References' },
    ['<C-k>'] = { vim.lsp.buf.signature_help, 'Go to Help' },
    ['[d'] = { prev, 'Go to Previous Diagnostic' },
    [']d'] = { next, 'Go to Next Diagnostic' },
  })

  -- Auto-format the buffer on save
  if client.server_capabilities.document_formatting then
    vim.cmd [[ 
      au BufWritePre <buffer> lua vim.lsp.buf.formatting({ async = true })
    ]]
  end

  -- Highlight occurrences of word under cursor
  if client.server_capabilities.documentHighlightProvider then
		vim.cmd [[ 
			au CursorHold <buffer> silent! lua vim.lsp.buf.document_highlight()
      au CursorHoldI <buffer> silent! lua vim.lsp.buf.document_highlight()
			au CursorMoved <buffer> silent! lua vim.lsp.buf.clear_references()
	  ]]
	end
end

return on_attach
