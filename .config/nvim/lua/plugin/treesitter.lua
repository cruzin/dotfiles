local status_ok, configs = pcall(require, 'nvim-treesitter.configs')
if not status_ok then
  require('utils').plugin_fail({ 'nvim-treesitter' })
  return
end

configs.setup({
  -- Installation
  auto_install = true,
  sync_install = false,
  ensure_installed = {
    'bash',
    'css',
    'html',
    'http',
    'javascript',
    'json',
    'lua',
    'python',
    'regex',
    'scss',
    'sql',
    'tsx',
    'typescript',
    'vim',
    'yaml',
  },
  -- Features
  autopairs = {
    enable = true,
  },
  highlight = {
    enable = true,
  },
  indent = {
    enable = true,
  },
})

