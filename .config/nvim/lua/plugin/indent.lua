local plugin = 'indent_blankline'
local indent_ok, indent = pcall(require, plugin)

if not indent_ok then
  require('utils').plugin_fail({ plugin })
  return
end

indent.setup({
  bufname_exclude = { '' }, -- disables in hover popups & new files
  char = '⸽',
  char_highlight_list = { 'Whitespace' },
  context_char = '⸽',
  context_highlight_list = { 'WinSeparator' },
  show_current_context = true,
  use_treesitter = true,
})
