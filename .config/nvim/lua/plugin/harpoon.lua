require('which-key').register({
  h = {
    name = 'Harpoon',
    a = { require('harpoon.mark').add_file, 'Add File' },
    h = { require('harpoon.ui').toggle_quick_menu, 'Toggle Harpoon UI' },
  },
  ['<backspace>'] = { '<C-^>', 'Go To Alternate File' },
  ['<leader>'] = { function()
    require('harpoon.ui').nav_file(1)
  end, 'Go To Main Harpoon File' },
}, { prefix = '<leader>' })
