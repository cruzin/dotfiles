local plugin = 'marks'
local marks_ok, marks = pcall(require, plugin )

if not marks_ok then
  require('utils').plugin_fail({ plugin })
  return
end

marks.setup({
  excluded_filetypes = { 'alpha', 'dashboard' },
})

require('which-key').register({
  m = {
    name = 'Marks',
    [','] = { 'Set next available alphabetical mark' },
    [';'] = { 'Toggle the next available mark at the current line' },
    [']'] = { 'Move to the next mark' },
    ['['] = { 'Move to the previous mark' },
    [':'] = { 'Preview a specific mark' },
  },
  ['dm-'] = { 'Delete all marks on the current line' },
  ['dm='] = { 'Delete the bookmark under the cursor' },
  ['dm<leader>'] = { 'Delete all marks in the current buffer' },
})
