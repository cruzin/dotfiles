local plugin = 'rest-nvim'
local rest_ok, rest = pcall(require, plugin)

if not rest_ok then
  require('utils').plugin_fail({ plugin })
  return
end

rest.setup()

require('which-key').register({
  r = {
    name = 'REST',
    r = { '<Plug>RestNvim<cr>', 'Run Request Under Cursor' },
    p = { '<Plug>RestNvimPreview<cr>', 'Preview Request cURL' },
    l = { '<Plug>RestNvimLast<cr>', 'Re-run Last Request' },
  }
}, { prefix = '<leader>' })
