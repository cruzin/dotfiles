local plugin = 'gitsigns'
local gs_ok, gs = pcall(require, plugin)

if not gs_ok then
  require('utils').plugin_fail({ plugin })
  return
end

gs.setup({
  current_line_blame_opts = {
    delay = 500,
    virt_text_pos = 'right_align',
  },
  preview_config = {
    border = 'rounded',
  },
  signs = {
    add = {
      hl = 'GitSignsAdd',
      text = '▏',
      numhl = 'GitSignsAddNr',
      linehl = 'GitSignsAddLn',
    },
    change = {
      hl = 'GitSignsChange',
      text = '▏',
      numhl = 'GitSignsChangeNr',
      linehl = 'GitSignsChangeLn',
    },
    delete = {
      hl = 'GitSignsDelete',
      text = '',
      numhl = 'GitSignsDeleteNr',
      linehl = 'GitSignsDeleteLn',
    },
    topdelete = {
      hl = 'GitSignsDelete',
      text = '',
      numhl = 'GitSignsDeleteNr',
      linehl = 'GitSignsDeleteLn',
    },
    changedelete = {
      hl = 'GitSignsChange',
      text = '▏',
      numhl = 'GitSignsChangeNr',
      linehl = 'GitSignsChangeLn',
    }
  },
  watch_gitdir = {
    interval = 1000,
    follow_files = true
  }
})

local defer = require('utils').defer

local function go_to_hunk(dir)
  local dirs = {
    prev = { keymap = '[h', fn = gs.next_hunk },
    next = { keymap = ']h', fn = gs.prev_hunk }
  }
  return function()
    if vim.wo.diff then return dirs[dir].keymap end
    defer(dirs[dir].fn)
    return '<Ignore>'
  end
end

require('which-key').register({
  g = {
    name = 'Git',
    B = { ':Gitsigns toggle_current_line_blame<cr>', 'Toggle line blame' },
  },
}, { prefix = '<leader>' })

require('which-key').register({
  [']h'] = { go_to_hunk('next'), 'Next Hunk', { expr = true } },
  ['[h'] = { go_to_hunk('prev'), 'Previous Hunk', { expr = true } },
})
