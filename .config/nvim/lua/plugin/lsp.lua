local mason_lsp_ok, mason_lsp = pcall(require, 'mason-lspconfig')
local lsp_ok, lsp = pcall(require, 'lspconfig')

if not mason_lsp_ok or not lsp_ok then
  require('utils').plugin_fail({ 'mason-lspconfig', 'lspconfig' })
  return
end

-- Custom icons for diagnostics --
local signs = { Error = '', Warn = '', Hint = '', Info = '' }
for type, icon in pairs(signs) do
  local hl = 'DiagnosticSign' .. type
  vim.fn.sign_define(hl, {
    text = icon,
    texthl = hl,
    numhl = hl
  })
end

vim.diagnostic.config({
  float = { border = 'rounded' },
  signs = true,
  virtual_lines = false,
  virtual_text = false,
})

vim.lsp.handlers['textDocument/hover'] = vim.lsp.with(vim.lsp.handlers.hover, {
  border = 'rounded',
})

vim.lsp.handlers['textDocument/signatureHelp'] = vim.lsp.with(vim.lsp.handlers.signature_help, {
  border = 'rounded',
})

vim.lsp.handlers['textDocument/publishDiagnostics'] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics,
  { update_in_insert = false }
)

-- Completion symbols
vim.lsp.protocol.CompletionItemKind = {
  '   (Text)',
  '   (Method)',
  '   (Function)',
  '   (Constructor)',
  ' ﴲ  (Field)',
  '[] (Variable)',
  '   (Class)',
  ' ﰮ  (Interface)',
  '   (Module)',
  ' 襁 (Property)',
  '   (Unit)',
  '   (Value)',
  ' 練 (Enum)',
  '   (Keyword)',
  '   (Snippet)',
  '   (Color)',
  '   (File)',
  '   (Reference)',
  '   (Folder)',
  '   (EnumMember)',
  ' ﲀ  (Constant)',
  ' ﳤ  (Struct)',
  '   (Event)',
  '   (Operator)',
  '   (TypeParameter)'
}

local lsp_flags = {
  -- This is the default in Nvim 0.7+
  debounce_text_changes = 150,
}

-- Set up code completion capabilities
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.insertReplaceSupport = true
capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities.textDocument.completion.completionItem.preselectSupport = true
capabilities.textDocument.completion.completionItem.resolveSupport = {
	properties = { 'documentation', 'detail', 'additionalTextEdits' },
}

-- Pull in our on_attach for servers
local on_attach = require('plugin.lsp.on_attach')

mason_lsp.setup({
  ensure_installed = {
    'bashls',
    'cssls',
    'cssmodules_ls',
    'emmet_ls',
    'eslint',
    'html',
    'jsonls',
    'sumneko_lua',
    'rust_analyzer',
    'tailwindcss',
    'tsserver',
    'yamlls',
  },
})

mason_lsp.setup_handlers({
  function(server_name)
    lsp[server_name].setup({
      capabilities = capabilities,
      flags = lsp_flags,
      on_attach = on_attach,
    })
  end
})

-- LANGUAGE-SPECIFIC SETTINGS --

-- Lua
lsp['sumneko_lua'].setup({
  settings = {
    Lua = {
      runtime = {
        version = 'LuaJIT',
      },
      diagnostics = {
        globals = { 'vim' },
      },
      workspace = {
        library = vim.api.nvim_get_runtime_file('', true),
      },
      telemetry = {
        enable = false,
      },
    },
  },
})

