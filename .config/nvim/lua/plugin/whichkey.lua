local plugin = 'which-key'
local wk_ok, wk = pcall(require, plugin)

if not wk_ok then
  require('utils').plugin_fail({ plugin })
  return
end

wk.setup({
  key_labels = {
    ['<leader>'] = vim.g.mapleader,
    ['<space>'] = 'SPC',
    ['<BS>'] = 'BS',
    ['<CR>'] = 'RET',
    ['<tab>'] = 'TAB'
  },
  icons = {
    breadcrumb = '',
    group = ' '
  },
  window = {
    border = 'single',
    margin = { 0, 0, 1, 0 }
  }
})

wk.register({
  ['?'] = { ':WhichKey<cr>', 'Keymaps' },
}, { prefix = '<leader>' })
