local plugin = 'lualine'
local lualine_ok, lualine = pcall(require, plugin)

if not lualine_ok then
  require('utils').plugin_fail({ plugin })
  return
end

local icons = require('nvim-web-devicons')
local fn = vim.fn

local branch = {
  'branch',
  icon = '',
  icons_enabled = true,
}

local diagnostics = {
  'diagnostics',
  sources = { 'nvim_diagnostic' },
  symbols = { error = ' ', warn = ' ', info = ' ', hint = ' ' },
}

local diff = {
  'diff',
  cond = function() return fn.winwidth(0) > 80 end
}

local mode = {
  'mode',
  fmt = function(str) return ' ▏' .. str end
}

local function progress()
  local current_line = fn.line('.')
  local total_lines = fn.line('$')
  --local chars = { '▯▯▯▯▯', '▮▯▯▯▯', '▮▮▯▯▯', '▮▮▮▯▯', '▮▮▮▮▯', '▮▮▮▮▮' }
  local chars = { '     ', '    ▮', '   ▮▮', '  ▮▮▮', ' ▮▮▮▮', '▮▮▮▮▮' }
  --local chars = { '▫▫▫▫▫', '▪▫▫▫▫', '▪▪▫▫▫', '▪▪▪▫▫', '▪▪▪▪▫', '▪▪▪▪▪' }
  local line_ratio = current_line / total_lines
  local index = math.ceil(line_ratio * #chars)
  return chars[index]
end

local filename = {
  'filename',
  file_status = true,
  fmt = function(filename)
    local icon = icons.get_icon(filename, fn.expand('%:e'))
    return icon and (icon .. ' ' .. filename) or filename
  end,
  path = 0, -- just show the file name
  symbols = {
    modified = '',
    readonly = '',
    unnamed = '-- no name --',
    newfile = '-- new --'
  },
}

lualine.setup({
  options = {
    always_divide_middle = true,
    disabled_filetypes = { 'alpha', 'dashboard', 'packer', 'NvimTree' },
    icons_enabled = true,
    component_separators = { left = '•', right = '•' },
    section_separators = { left = '', right = '' },
    theme = 'tokyonight'
  },
  sections = {
    lualine_a = { mode },
    lualine_b = { filename },
    lualine_c = { diagnostics },
    lualine_x = { diff },
    lualine_y = { branch },
    lualine_z = { progress }
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = { filename },
    lualine_c = {},
    lualine_x = {},
    lualine_y = {},
    lualine_z = {}
  },
  extensions = {},
})
