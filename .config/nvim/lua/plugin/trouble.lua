local plugin = 'trouble'
local trouble_ok, trouble = pcall(require, plugin)

if not trouble_ok then
  require('utils').plugin_fail({ plugin })
  return
end

trouble.setup({
  auto_fold = true,
  mode = 'document_diagnostics',
  use_diagnostic_signs = true,
})

local function toggle_workspace_diagnostics()
  vim.cmd [[ TroubleToggle workspace_diagnostics ]]
  require('lsp_lines').toggle()
end

local function toggle_document_diagnostics()
  vim.cmd [[ TroubleToggle document_diagnostics ]]
  require('lsp_lines').toggle()
end

require('which-key').register({
  l = {
    name = 'LSP',
    D = { toggle_workspace_diagnostics, 'Workspace Diagnostics' },
    d = { toggle_document_diagnostics, 'File Diagnostics' }
  }
}, { prefix = '<leader>' })
