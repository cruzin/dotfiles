local cmp_ok, cmp = pcall(require, 'cmp')
local snip_ok, snip = pcall(require, 'luasnip')

if not cmp_ok or not snip_ok then
  require('utils').plugin_fail({ 'cmp', 'luasnip' })
  return
end

local function check_backspace()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match '%s' == nil
end

require('luasnip.loaders.from_vscode').lazy_load()

local compare = cmp.config.compare
local kind_icons = require('utils').kind_icons

cmp.setup({
  snippet = {
    expand = function(args)
      snip.lsp_expand(args.body)
    end
  },
  formatting = {
    fields = { 'kind', 'abbr', 'menu' },
    format = function(entry, vim_item)
      vim_item.kind = string.format('%s', kind_icons[vim_item.kind].icon)
      vim_item.menu = ({
        nvim_lsp = '[LSP]',
        nvim_lua = '[API]',
        luasnip = '[Snip]',
        buffer = '[Buf]',
        path = '[Path]',
      })[entry.source.name]
      return vim_item
    end
  },
  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered(),
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif snip.expandable() then
        snip.expand({})
      elseif snip.expand_or_jumpable() then
        snip.expand_or_jump()
      elseif check_backspace() then
        fallback()
      else
        fallback()
      end
    end, {
      'i',
      's',
    }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif snip.jumpable(-1) then
        snip.jump(-1)
      else
        fallback()
      end
    end, {
      'i',
      's',
    }),
  }),
  sources = {
    { name = 'nvim_lua', priority = 8 },
    { name = 'nvim_lsp', priority = 8, max_item_count = 20 },
    { name = 'luasnip', priority = 7, max_item_count = 5, keyword_length = 1 },
    { name = 'buffer', priority = 6, max_item_count = 10, keyword_length = 3 },
    { name = 'path', priority = 6 }
  },
  sorting = {
    priority_weight = 2,
    comparators = {
      compare.offset,
      compare.exact,
      compare.score,
      compare.recently_used,
      compare.locality,
      compare.sort_text,
      compare.length,
      compare.order,
    },
  },
  confirm_opts = {
    behavior = cmp.ConfirmBehavior.Replace,
    select = false,
  },
})

cmp.setup.cmdline('/', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = {
    { name = 'buffer' },
  },
})

cmp.setup.cmdline(':', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = cmp.config.sources({
    { name = 'path' },
  }, {
    { name = 'cmdline' }
  })
})
