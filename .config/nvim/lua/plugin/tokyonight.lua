local plugin = 'tokyonight'
local tn_ok, tn = pcall(require, plugin)

if not tn_ok then
  require('utils').plugin_fail({ plugin })
  return
end

tn.setup({
  dim_inactive = true,
  lualine_bold = true,
  style = 'night',
  styles = {
    keywords = { italic = false }
  },
  terminal_colors = false,
  on_highlights = function(highlights, _)
    highlights.WinSeparator = { fg = '#9ece6a' } -- Make the separator more obvious
  end
})

vim.cmd [[colorscheme tokyonight]]
