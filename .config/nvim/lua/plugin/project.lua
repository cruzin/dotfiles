local plugin = 'project_nvim'
local p_ok, p = pcall(require, plugin)

if not p_ok then
  require('utils').plugin_fail({ plugin })
  return
end

p.setup({
  show_hidden = true,
  silent_chdir = true,
  sync_root_with_cwd = true,
  respect_buf_cwd = true,
  update_focused_file = {
    enable = true,
    update_root = true
  },
})
