local plugin = 'FTerm'
local fterm_ok, fterm = pcall(require, plugin)
if not fterm_ok then
  require('utils').plugin_fail({ plugin })
  return
end

vim.api.nvim_create_user_command('FTermToggle', fterm.toggle, { bang = true })

fterm.setup({
  border = 'rounded',
})

require('which-key').register({
  ['<C-`>'] = { ':FTermToggle<cr>', 'Toggle Terminal' },
})

require('which-key').register({
  ['<C-`>'] = { '<cmd>FTermToggle<cr>', 'Toggle Terminal' },
}, { mode = 't' })
