local plugin = 'symbols-outline'
local symbols_ok, symbols = pcall(require, plugin)

if not symbols_ok then
  require('utils').plugin_fail({ plugin })
  return
end

symbols.setup({
  relative_width = false,
  symbols = require('utils').kind_icons,
  width = 50,
})
