local plugin = 'diffview'
local dv_ok, dv = pcall(require, plugin)

if not dv_ok then
  require('utils').plugin_fail({ plugin })
  return
end

dv.setup({
  file_panel = {
    win_config = {
      width = 40
    }
  }
})

require('which-key').register({
  g = {
    name = 'Git',
    D = { ':DiffviewOpen main<cr>', 'Diff With Main Branch' },
    H = { ':DiffviewFileHistory<cr>', 'All Files Diff History' },
    d = { ':DiffviewOpen<cr>', 'Diff With Current Branch' },
    h = { ':DiffviewFileHistory %<cr>', 'Current File Diff History' },
    x = { ':DiffviewClose<cr>', 'Close Diff View' },
  },
}, { prefix = '<leader>' })
