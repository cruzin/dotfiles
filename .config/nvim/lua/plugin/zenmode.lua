local plugin = 'zen-mode'
local zenmode_ok, zenmode = pcall(require, plugin)

if not zenmode_ok then
  require('utils').plugin_fail({ plugin })
  return
end

zenmode.setup({
  window = {
    backdrop = 0.85,
    height = 0.95,
    width = 80
  },
  plugins = {
    gitsigns = { enabled = true },
    kitty = {
      enabled = true,
      font = '+1'
    },
  }
})

require('which-key').register({
  z = { ':ZenMode<cr>', 'Zen Mode' },
}, { prefix = '<leader>' })
