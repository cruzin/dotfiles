local plugin = 'alpha'
local status_ok, alpha = pcall(require, plugin)
if not status_ok then
  require('utils').plugin_fail({ plugin })
  return
end

local max_width = 55
local hl1 = 'String'
local hl2 = 'Label'

local function button(sc, txt, keybind, keybind_opts)
  local sc_ = sc:gsub('%s', ''):gsub('SPC', '<leader>')

  local opts = {
    position = 'center',
    shortcut = sc,
    cursor = 5,
    width = max_width,
    align_shortcut = 'right',
    hl_shortcut = 'Normal',
    hl = hl2
  }

  if keybind then
    keybind_opts = vim.F.if_nil(keybind_opts, { noremap = true, silent = true, nowait = true })
    opts.keymap = { 'n', sc_, keybind, keybind_opts }
  end

  local function on_press()
    local key = vim.api.nvim_replace_termcodes(keybind or sc_ .. '<Ignore>', true, false, true)
    vim.api.nvim_feedkeys(key, 't', false)
  end

  return {
    type = 'button',
    val = txt,
    on_press = on_press,
    opts = opts
  }
end

-- Text header
local header = {
  type = 'text',
  val = {
    [[                                        ]],
    [[ ██████████████████████████████████████ ]],
    [[ █▄ ▀█▄ ▄█▄ ▄▄ █ ▄▄ █▄ █ ▄█▄ ▄█▄ ▀█▀ ▄█ ]],
    [[ ██ █▄▀ ███ ▄█▀█ ██ ██▄▀▄███ ███ █▄█ ██ ]],
    [[ ▀▄▄▄▀▀▄▄▀▄▄▄▄▄▀▄▄▄▄▀▀▀▄▀▀▀▄▄▄▀▄▄▄▀▄▄▄▀ ]],
    [[                                        ]],
  },
  opts = {
    position = 'center',
    type = 'ascii',
    hl = hl1,
  }
}

-- Button groups
local quick_link_btns = {
  type = 'group',
  val = {
    { type = 'text', val = 'QUICK LINKS', opts = { hl = hl1, position = 'center' } },
    { type = 'padding', val = 1 },
    button('n', '  New File', ':ene <BAR> startinsert<CR>'),
    button('SPC f f', '  Find File', ':Telescope file_browser<CR>'),
    button('SPC f g', '  Find Text', ':Telescope live_grep<CR>'),
    button('SPC r', '  Recent Files', ':Telescope oldfiles<CR>'),
    button('SPC p', '  Projects', ':cd ~/Projects/<CR> :Telescope repo list<CR>'),
  },
  opts = {
    hl = hl2,
    position = 'center'
  }
}

local config_btns = {
  type = 'group',
  val = {
    { type = 'text', val = 'CONFIGS', opts = { hl = hl1, position = 'center' } },
    { type = 'padding', val = 1 },
    button('SPC c', '  Config Files', ':cd ~/.config<CR> :Telescope file_browser<CR>'),
    button('SPC c 1', '    Neovim', ':cd ~/.config/nvim/<CR> :e init.lua<CR>'),
    button('SPC c 2', '    Qtile', ':cd ~/.config/qtile/<CR> :e config.py<CR>'),
    button('SPC c 3', '    Kitty', ':e ~/.config/kitty/kitty.conf<CR>'),
    button('SPC c 4', '    tmux', ':e ~/.tmux.conf.local<CR>'),
  },
  opts = {
    position = 'center',
    hl = hl2
  }
}

local misc_btns = {
  type = 'group',
  val = {
    button('q', '  Quit', ':qa<CR>'),
  },
  opts = {
    position = 'center',
  }
}

-- Footer
local utils = require('utils')
local plugins = utils.get_plugins_list()
local plugins_str = table.concat(plugins, ', ')
local formatted_plugins = utils.wrap_line(plugins_str, max_width)

local footer1 = {
  type = 'text',
  val = '  ' .. (#plugins) .. ' plugins installed',
  opts = {
    position = 'center',
    hl = hl1
  }
}

local footer2 = {
  type = 'text',
  val = formatted_plugins,
  opts = {
    position = 'center',
    hl = 'Normal'
  }
}

local config = {
  layout = {
    { type = 'padding', val = 2 },
    header,
    { type = 'padding', val = 3 },
    quick_link_btns,
    { type = 'padding', val = 1 },
    config_btns,
    { type = 'padding', val = 2 },
    misc_btns,
    { type = 'padding', val = 3 },
    footer1,
    { type = 'padding', val = 1 },
    footer2
  },
  opts = {
    noautocmd = false,
    redraw_on_resize = true,
  }
}

alpha.setup(config)
