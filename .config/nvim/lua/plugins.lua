local function ensure_packer()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({
      'git',
      'clone',
      '--depth',
      '1',
      'https://github.com/wbthomason/packer.nvim',
      install_path
    })
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local plugin = 'packer'
local packer_bootstrap = ensure_packer()
local packer_ok, packer = pcall(require, plugin)
if not packer_ok then
  require('utils').plugin_fail({ plugin })
  return
end

-- Have packer use a popup window
packer.init({
  display = {
		open_fn = function()
      return require('packer.util').float({ border = 'rounded' })
    end,
    prompt_border = 'rounded'
  }
})

packer.startup(function(use)
  use 'wbthomason/packer.nvim' -- packer manages itself

  -- Load these first --
  use 'lewis6991/impatient.nvim'
  use 'nathom/filetype.nvim' -- replaces slow filetype.vim
  use 'nvim-lua/plenary.nvim'
  use 'nvim-lua/popup.nvim'
  use 'nvim-tree/nvim-web-devicons'

  -- TESTING ZONE --------------------------------------------------------------

  -- LSP symbols --
  use { 'simrat39/symbols-outline.nvim', config = 'require("plugin.symbols")' }

  -- END TESTING ZONE ----------------------------------------------------------

  -- Theme --
  use { 'folke/tokyonight.nvim', config = 'require("plugin.tokyonight")' }

  -- Dashboard --
  use { 'goolord/alpha-nvim', config = 'require("plugin.alpha")' }

  -- Focus mode --
  use { 'folke/zen-mode.nvim', config = 'require("plugin.zenmode")' }

  -- LSP & LSP management --
  use 'folke/lsp-colors.nvim' -- add missing LSP highlight group colors to old themes
  use 'neovim/nvim-lspconfig'
  use 'williamboman/mason.nvim'
  use {
    'williamboman/mason-lspconfig.nvim',
    config = function()
      require('mason').setup()
      require('plugin.lsp')
    end
  }

  -- Render LSP diagnostics with virtual lines --
  use {
    'https://git.sr.ht/~whynothugo/lsp_lines.nvim',
    config = 'require("lsp_lines").setup()'
  }

  -- Shows a window with all LSP diagnostics --
  use { 'folke/trouble.nvim', config = 'require("plugin.trouble")' }

  -- Code completion --
  use 'hrsh7th/nvim-cmp'
  use { 'hrsh7th/cmp-nvim-lsp', after = 'nvim-cmp' }
  use { 'hrsh7th/cmp-buffer', after = 'cmp-nvim-lsp' }
  use { 'hrsh7th/cmp-path', after = 'cmp-buffer' }
  use { 'hrsh7th/cmp-cmdline', after = 'cmp-path' }
  use { 'saadparwaiz1/cmp_luasnip', after = 'cmp-cmdline' }
  use {
    'L3MON4D3/LuaSnip',
    after = 'cmp_luasnip',
    config = 'require("plugin.cmp")',
  }

  -- Code highlighting & more --
  use {
    'nvim-treesitter/nvim-treesitter',
    config = 'require("plugin.treesitter")',
    run = 'require("nvim-treesitter.install").update({ with_sync = true })'
  }
  use { 'nvim-treesitter/nvim-treesitter-context', after = 'nvim-treesitter' }

  -- List searching --
  use {
    'nvim-telescope/telescope.nvim',
    config = 'require("plugin.telescope")',
    requires = {
      'nvim-lua/plenary.nvim',
      'nvim-lua/popup.nvim',
      'nvim-tree/nvim-web-devicons'
    },
    --tag = '0.1.0'
  }

  -- DAP --
  use 'theHamsta/nvim-dap-virtual-text'
  use 'nvim-telescope/telescope-dap.nvim'
  use {
    'microsoft/vscode-js-debug',
    opt = true,
    run = 'pnpm i --legacy-peer-deps && pnpm compile'
  }
  use { 'mxsdev/nvim-dap-vscode-js', requires = 'microsoft/vscode-js-debug' }
  use { 'mfussenegger/nvim-dap', config = 'require("plugin.dap")' }

  -- Project management --
  use 'cljoly/telescope-repo.nvim'
  use { 'ahmedkhalf/project.nvim', config = 'require("plugin.project")' }

  -- File management --
  use {
    'nvim-telescope/telescope-file-browser.nvim',
    requires = 'nvim-telescope/telescope.nvim'
  }

  -- Harpoon... it's blazing fast! --
  use { 'ThePrimeagen/harpoon', config = 'require("plugin.harpoon")' }

  -- Fancy statusbar --
  use { 'nvim-lualine/lualine.nvim', config = 'require("plugin.lualine")' }

  -- Show changes in gutter for Git-controlled files --
  use {
    'lewis6991/gitsigns.nvim',
    config = 'require("plugin.gitsigns")',
    event = 'BufRead',
    tag = 'release'
  }

  -- Diff view --
  use {
    'sindrets/diffview.nvim',
    config = 'require("plugin.diffview")',
    requires = 'nvim-lua/plenary.nvim'
  }

  -- Show keymap possibilities --
  use { 'folke/which-key.nvim', config = 'require("plugin.whichkey")' }

  -- Show marks in the gutter --
  use { 'chentoast/marks.nvim', config = 'require("plugin.marks")' }

  -- Terminal --
  use { 'numToStr/FTerm.nvim', config = 'require("plugin.fterm")' }

  -- Quick commenting --
  use { 'numToStr/Comment.nvim', config = 'require("Comment").setup()' }

  -- Highlight specific comment types --
  use { 'folke/todo-comments.nvim', config = 'require("todo-comments").setup()' }

  -- Show indentation lines --
  use { 'lukas-reineke/indent-blankline.nvim', config = 'require("plugin.indent")' }

  -- Faster jumping --
  use { 'rlane/pounce.nvim', config = 'require("plugin.pounce")' }

  -- REST client --
  use { 'NTBBloodbath/rest.nvim', config = 'require("plugin.rest")' }

  -- Consistent tmux/nvim split navigation --
  use 'christoomey/vim-tmux-navigator'

  -- Notifications --
  use 'rcarriga/nvim-notify'

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)

return packer
