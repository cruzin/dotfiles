local plugin = 'which-key'
local wk_ok, wk = pcall(require, plugin)

vim.keymap.set('', '<Space>', '<Nop>')
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

if not wk_ok then
  require('utils').plugin_fail({ plugin })
  return
end

-- MODES --
-- normal = 'n'
-- insert = 'i'
-- visual = 'v'
-- visual_block = 'x'
-- term = 't'
-- command = 'c'
-- operator-pending = 'o'

---- LEADER PREFIX -------------------------------------------------------------

wk.register({
  s = {
    name = 'Split',
    s = { ':sp<cr>', 'Horizontal' },
    v = { ':vsp<cr>', 'Vertical' },
    c = { ':close<cr>', 'Close' },
    h = { '<C-w>h', 'Focus Left' },
    j = { '<C-w>j', 'Focus Up' },
    k = { '<C-w>k', 'Focus Down' },
    l = { '<C-w>l', 'Focus Right' },
  },
  q = { ':bd<cr>', 'Close Buffer' },
}, { prefix = '<leader>' })

---- MISCELLANEOUS -------------------------------------------------------------

wk.register({
  -- BUFFERS --
  ['<A-l>'] = { ':bnext<cr>', 'Next Buffer' },
  ['<A-h>'] = { ':bprevious<cr>', 'Previous Buffer' },
})

-- VISUAL BLOCK --
wk.register({
  ['<'] = { '<gv', 'Outdent' },
  ['>'] = { '>gv', 'Indent' },
  -- Keeps the initially yanked text available instead of swapping it with 
  -- the replaced text!
  p = { '"_dP', 'Paste' },
}, { mode = 'x' })
