local options = {
  -- FILE MANAGEMENT --
  autoread = true,
  --backupdir = '~/.cache/vim',
  encoding = 'utf-8',
  swapfile = false,

  -- INDENTATION
  autoindent = true,
  expandtab = true,
  shiftwidth = 2,
  smartindent = true,
  softtabstop = 2,
  tabstop = 2,

  -- NUMBER COLUMN --
  number = true,
  relativenumber = true,

  -- ROWS/COLUMNS --
  colorcolumn = '80',
  cursorline = true,
  signcolumn = 'yes',

  -- SCROLLING --
  scrolloff = 8,

  -- SEARCH --
  hlsearch = false, -- don't highlight search results
  ignorecase = true, -- ignore search case
  incsearch = true,
  showmatch = true,
  smartcase = true,

  -- SPLIT --
  splitbelow = true,
  splitright = true,

  -- MODE/TAB/STATUS LINE --
  laststatus = 3, -- single, global statusline
  modeline = true,
  showmode = false,
  showtabline = 0, -- don't show tabline

  -- MISCELLANEOUS --
  clipboard = 'unnamedplus', -- system copy/paste
  completeopt = { 'menu', 'menuone', 'noselect' },
  hidden = true, -- allow hidden buffers
  mouse = 'v', -- allow mouse in visual mode
  termguicolors = true,
  updatetime = 1750, -- increase update frequency (for CursorHold mainly)
  wrap = false, -- don't wrap long lines
}

for k, v in pairs(options) do
  vim.opt[k] = v
end

vim.opt.iskeyword:append('-') -- dashed words are whole words
