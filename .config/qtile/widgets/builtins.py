from os.path import expanduser
from subprocess import check_output

from libqtile.lazy import lazy
from libqtile.widget import \
    CheckUpdates, GenPollText, GroupBox, Spacer, \
    Systray, TextBox, WidgetBox

from classes import Helpers, Palette

dpi = Helpers.dpi
theme = "mocha"
colors = Palette.colors[theme]

Caffeine = GenPollText(
    fmt="{}",
    fontsize=dpi(18),
    func=lambda: check_output(
        expanduser("~/.local/bin/caffeine.sh")
    ).decode("utf-8"),
    mouse_callbacks={
        "Button1": lazy.spawn(
            expanduser("~/.local/bin/caffeine.sh --toggle")
        )
    },
    update_interval=1,
)

Extras = WidgetBox(
    widgets=[
        TextBox(
            background=colors["crust"],
            foreground=colors["green"],
            text=" " + Helpers.get_os_release(),
        ),
        TextBox(
            background=colors["crust"],
            foreground=colors["yellow"],
            text=" " + Helpers.get_kernel_release(),
        ),
        Systray(
            background=colors["crust"],
        ),
        Spacer(
            background=colors["crust"],
            length=6,
        ),
    ],
    fontsize=dpi(16),
    text_closed=" ",
    text_open=" ",
)

Groupbox = GroupBox(
    active=colors["overlay0"],
    block_highlight_text_color=colors["text"],
    borderwidth=0,
    disable_drag=True,
    fontsize=dpi(18),
    hide_unused=True,
    inactive=colors["base"],
    padding=dpi(2),
    rounded=False,
    urgent_text=colors["red"],
)

Updates = CheckUpdates(
    background=colors["green"],
    colour_have_updates=colors["base"],
    custom_command="zypper lu",
    custom_command_modify=lambda x: x - 4,
    display_format="{updates} ",
    mouse_callbacks={
        "Button1": lazy.spawn("kitty -e sudo zypper dup"),
    },
    padding=0,
    update_interval=300,
)
