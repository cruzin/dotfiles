from os.path import expanduser

from libqtile.bar import Bar, Gap
from libqtile.config import Screen
from libqtile.lazy import lazy
from libqtile.widget import \
    Chord, Clipboard, Clock, CPU, CurrentLayoutIcon, \
    Image, Memory, Prompt, Spacer, Wallpaper

from widgets.owm import OpenWeatherMap
from widgets.volume import MyPulseVolume
from widgets.window_indicators import WindowIndicators
from widgets.builtins import Caffeine, Extras, Groupbox, Updates

from classes import Helpers, Palette

dpi = Helpers.dpi
theme = "mocha"
colors = Palette.colors[theme]

layout_icon_paths = "" if theme == "mocha" else [
    expanduser("~/.config/qtile/layout-icons/gruvbox-dark0")]
opensuse_icon = "Button-monochrome" if theme == "mocha" else "Button-monochrome-white"

widget_defaults = dict(
    background=colors["surface0"],
    font="Hack Nerd Font",
    fontsize=dpi(12),
    foreground=colors["text"],
    padding=dpi(10),
)
extension_defaults = widget_defaults.copy()

num_screens = int(Helpers.get_num_screen())
screens = []

for i in range(0, num_screens):
    screens.extend([
        Screen(
            top=Gap(2),
            left=Gap(2),
            right=Gap(2),
            bottom=Bar([
                Spacer(
                    background=colors["green"],
                    length=2,
                ),
                Image(
                    background=colors["green"],
                    filename=expanduser(
                        f"~/.local/share/opensuse/{opensuse_icon}.png"),
                    margin=dpi(4),
                ),
                Updates,
                CurrentLayoutIcon(
                    custom_icon_paths=layout_icon_paths,
                    padding=dpi(4),
                    scale=0.55,
                ),
                Chord(
                    chords_colors={
                        "Audio": (colors["peach"], colors["base"]),
                        "Gnome": (colors["mauve"], colors["base"]),
                        "Grow": (colors["blue"], colors["base"]),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                Prompt(
                    bell_style="visual",
                    cursor_color=colors["red"],
                    ignore_dups_history=True,
                    prompt=" ",
                    visual_bell_color=colors["red"],
                ),
                Clipboard(
                    blacklist=["1password", "1Password"],
                    fmt=" {}",
                    foreground=colors["flamingo"],
                ),
                Spacer(),
                Groupbox,
                Spacer(),
                WindowIndicators(
                    fontsize=dpi(9),
                    indicator="",
                ),
                MyPulseVolume(
                    foreground=colors["mauve"],
                    get_volume_command="pamixer --get-volume-human",
                    limit_max_volume=True,
                    mute_command="pamixer -m",
                    step=4,
                    volume_app="pamixer",
                    volume_down_command="pamixer -d 4",
                    volume_up_command="pamixer -i 4",
                ),
                OpenWeatherMap(
                    api_key="b8c0a2258d0134fb50533560dfb89a73",
                    foreground=colors["sapphire"],
                    format="{icon} {temp:.0f}{temp_units}",
                    latitude=30.2,
                    longitude=-97.7,
                    mouse_callbacks={
                        "Button1": lazy.spawn("gnome-weather"),
                    },
                    units="imperial",
                ),
                Memory(
                    foreground=colors["teal"],
                    format=" {MemUsed:.2f}{mm}",
                    measure_mem="G",
                ),
                CPU(
                    foreground=colors["green"],
                    format=" {load_percent:.0f}%",
                ),
                Clock(
                    foreground=colors["yellow"],
                    format=" %a %B %d",
                    mouse_callbacks={
                        "Button1": lazy.spawn("gnome-calendar"),
                    },
                ),
                Clock(
                    foreground=colors["peach"],
                    format=" %I:%M %p",
                ),
                Spacer(
                    length=6,
                ),
                Extras,
                Wallpaper(
                    directory=expanduser("~/Pictures/Wallpapers/"),
                    fontsize=dpi(18),
                    foreground=colors["text"],
                    label="",
                    random_selection=True,
                ),
                Caffeine,
                Spacer(
                    length=6,
                ),
                Spacer(
                    background=colors["green"],
                    length=4,
                ),
            ],
                background=colors["surface2"],
                # border_color=colors["green"],
                margin=[dpi(2), 0, 0, 0],
                opacity=0.888888880,
                size=dpi(24),
            ),
        )
    ])
